<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $idorder
 * @property int $idproduct
 * @property int $op_quantity
 * @property int $op_total
 * @property string $created_at
 * @property string $updated_at
 * @property CpTorder $cpTorder
 * @property CpTproduct $cpTproduct
 */
class OrderProduct extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cp_torder-product';

    /**
     * @var array
     */
    protected $fillable = ['idorder', 'idproduct', 'op_quantity', 'op_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cpTorder()
    {
        return $this->belongsTo('App\Order', 'idorder');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cpTproduct()
    {
        return $this->belongsTo('App\Order', 'idproduct');
    }
}
