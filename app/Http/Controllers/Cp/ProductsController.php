<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Products;
use DB;

class ProductsController extends Controller
{
    public function index()
    {
        $data = Products::get();
        return $data;
    }
}
