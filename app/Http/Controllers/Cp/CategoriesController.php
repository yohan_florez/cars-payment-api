<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Categories;
use DB;

class CategoriesController extends Controller
{
    public function index()
    {
        $data = Categories::get();
        return $data;
    }
}
