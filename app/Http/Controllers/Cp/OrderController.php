<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Order;
use App\OrderProduct;
use DB;

class OrderController extends Controller
{
    public function store(Request $request)
    {
    	$data = new Order();
    	$data->or_total = $request->or_total;
    	if($data->save()){
            foreach ($request->products as $p) {
                $orderProduct = new OrderProduct();
                $orderProduct->idorder = $data->id;
                $orderProduct->op_quantity = $p['pr_quantity'];
                $orderProduct->op_total = $p['priceItemsTotal'];
                $orderProduct->idproduct =$p['id'];
                $orderProduct->save();   
            }
            return ['status' => true, 'data' => $data ];	      
        }else
            return ['status' => false];
    }
}
 