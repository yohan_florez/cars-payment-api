<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $idcategory
 * @property string $pr_img
 * @property string $pr_name
 * @property string $pr_description
 * @property int $pr_price
 * @property int $pr_quantity
 * @property string $created_at
 * @property string $updated_at
 * @property CpTcategory $cpTcategory
 * @property CpTorderProduct[] $cpTorderProducts
 */
class Products extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cp_tproducts';

    /**
     * @var array
     */
    protected $fillable = ['idcategory', 'pr_img', 'pr_name', 'pr_description', 'pr_price', 'pr_quantity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cpTcategory()
    {
        return $this->belongsTo('App\Categories', 'idcategory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cpTorderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'idproduct');
    }
}
