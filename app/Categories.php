<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $ct_name
 * @property string $created_at
 * @property string $updated_at
 * @property CpTproduct[] $cpTproducts
 */
class Categories extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cp_tcategories';

    /**
     * @var array
     */
    protected $fillable = ['ct_name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cpTproducts()
    {
        return $this->hasMany('App\Products', 'idcategory');
    }
}
