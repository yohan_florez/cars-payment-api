<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $idcustomer
 * @property int $idprofessional
 * @property string $lg_email
 * @property string $lg_password
 * @property string $lg_role
 * @property string $created_at
 * @property string $updated_at
 * @property InTcustomer $inTcustomer
 * @property InTprofessional $inTprofessional
 */
class Login extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'in_tlogin';

    /**
     * @var array
     */
    protected $fillable = ['idcustomer', 'idprofessional', 'lg_email', 'lg_password', 'lg_role', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inTcustomer()
    {
        return $this->belongsTo('App\InTcustomer', 'idcustomer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inTprofessional()
    {
        return $this->belongsTo('App\InTprofessional', 'idprofessional');
    }
}
