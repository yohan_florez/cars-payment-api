<?php

namespace App\Lib;
use Intervention\Image\Facades\Image as Image;

class Utilities{

  public function saveImage($image, $thumb = false, $size = 250){
      $image = str_replace('data:image/png;base64,', '', $image);
      $image = str_replace('data:image/jpg;base64,', '', $image);
      $image = str_replace('data:image/jpeg;base64,', '', $image);
      $image = str_replace('data:image/gif;base64,', '', $image);
      if (strpos($image, 'images'))
          return $image;
      $name = \Uuid::generate()->string . ".jpg";
      $output_file = public_path('images') . '/' . $name;
      file_put_contents($output_file, base64_decode($image));
      chmod($output_file, 0777);
      if($thumb)
          $this->saveThumbnails($output_file, $size);
      return $name;
  }

  public function completePathImages($imageName){
    return env('AWS_BUCKET') . $imageName;
  }

  public function saveThumbnails($output_file, $size){
      $nameThumb = $output_file . "-thumb.jpg";
      Image::make($output_file)->resize($size, null, function ($constraint) {
          $constraint->aspectRatio();
      })->save($nameThumb);
      return $output_file . $nameThumb;
  }

  private function randomNumbers($min, $max){
      return mt_rand($min, $max);
  }

}
