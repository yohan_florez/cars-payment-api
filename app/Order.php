<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $or_total
 * @property string $created_at
 * @property string $updated_at
 * @property CpTorderProduct[] $cpTorderProducts
 */
class Order extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cp_torder';

    /**
     * @var array
     */
    protected $fillable = ['or_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cpTorderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'idorder');
    }
}
