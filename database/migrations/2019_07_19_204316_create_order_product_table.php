<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cp_torder-product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('op_quantity');
            $table->integer('op_total');
            $table->unsignedInteger('idorder')->nullable(); 
            $table->foreign('idorder')->references('id')->on('cp_torder');
            $table->unsignedInteger('idproduct')->nullable(); 
            $table->foreign('idproduct')->references('id')->on('cp_tproducts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cp_torder-product');
    }
}
