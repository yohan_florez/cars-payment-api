<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cp_tproducts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pr_img');
            $table->string('pr_name');
            $table->string('pr_description');
            $table->integer('pr_price');
            $table->integer('pr_quantity');
            $table->unsignedInteger('idcategory')->nullable(); 
            $table->foreign('idcategory')->references('id')->on('cp_tcategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cp_tproducts');
    }
}