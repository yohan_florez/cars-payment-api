<?php

use Illuminate\Http\Request;

Route::group([ 'prefix' => 'Cp'], function () {
	
	Route::resource('products', 'Cp\ProductsController');
	Route::resource('categories', 'Cp\CategoriesController');
	Route::post('order', 'Cp\OrderController@store');

});